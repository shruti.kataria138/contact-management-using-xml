import java.awt.Container;
import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
class MyContactManager extends JFrame implements ActionListener
{
    JButton add,delete,update,search,cancel;
    static JTable contacttable;
    static DefaultTableModel TModel;
    JTextField searchbox;
    String fullDB;
    MyContactManager(String title)
    {
        super(title);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we)
            {
                System.exit(0);
            }
        });
        Container contentpane=getContentPane();
        contentpane.setLayout(new FlowLayout());
        add=new JButton("Add New Contact");
        delete=new JButton("Delete");
        update=new JButton("Update");
        search=new JButton("Search");
        searchbox=new JTextField(10);
        contentpane.add(add);
        contentpane.add(delete);
        contentpane.add(update);
        contentpane.add(search);
        contentpane.add(searchbox);
        add.addActionListener(this);
        delete.addActionListener(this);
        update.addActionListener(this);
        search.addActionListener(this);
        TModel=new DefaultTableModel();
        contacttable=new JTable(TModel);
        JScrollPane jsp=new JScrollPane(contacttable,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
        ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        contentpane.add(jsp);
        loadData();
        setBounds(500, 300, 1200, 450);
    }
    public void loadData()
    {
        String colheads[]={"name","phone_number","email","dob","gender","address"};
        for(int i=0;i<colheads.length;i++)
        {
        String tag=colheads[i];
        String startTag="<"+tag+">";
        String endTag="</"+tag+">";
        File code=new File("C:\\Users\\Shruti\\OneDrive\\Desktop\\SL\\Advance Java\\Contact Management\\database.xml");
            try
            {
                FileInputStream fis=new FileInputStream(code);
                BufferedReader br=new BufferedReader(new InputStreamReader(fis));
                String line;
                Vector <String> retrievedText=new Vector <String>();
                int start=0;
                while((line=br.readLine())!=null)
                {
                    fullDB=fullDB+=line;
                    line=line.trim();
                    if(line.contains(startTag)&&line.contains(endTag))
                    {
                        int startI=startTag.length();
                        int endI=endTag.length();
                        endI=line.length()-endI;
                        retrievedText.add(line.substring(startI,endI)+"\n");
                        continue;
                    }
                    if(line.contains(startTag))
                    {
                        start=1;
                        continue;
                        
                    }
                        
                    if(line.contains(endTag))
                    {
                        start=0;
                    }    
                    if(start==1)
                        retrievedText.add(line+"\n");
                }
                TModel.addColumn(tag,retrievedText);
                br.close();
                fis.close();
            }
            catch(Exception ioe)
            {
                System.out.println(ioe.getMessage());
            }
        }
    }
    

    
    public void actionPerformed(ActionEvent ae) {
        int rowCount = TModel.getRowCount();
        int columnCount = TModel.getColumnCount();
        if (ae.getSource() == add) 
        {
            AddContactForm acf = new AddContactForm(this, "Add New Contact", TModel, contacttable);
            acf.setVisible(true);

        }
        if (ae.getSource() == delete) 
        {
            int selectedRow = contacttable.getSelectedRow();
            TModel.removeRow(selectedRow);
            reloadData();
        } 
        if (ae.getSource() == update) 
        {
            int cellMatched = 0;
            int edupCounter=0,mdupCounter=0;
            for (int i = 0; i < rowCount; i++) 
            {
                for (int j = 0; j < columnCount; j++) 
                {
                    String cellvalue = ((String) contacttable.getValueAt(i, j)).trim();
                    cellvalue.trim();
                    switch (j) {
                        case 0:
                            String nameRegex = "^[a-zA-Z]+\\s[a-zA-Z]*$";
                            Pattern namePattern = Pattern.compile(nameRegex);
                            Matcher nameMatcher = namePattern.matcher(cellvalue);
                            if (nameMatcher.matches()) {
                                cellMatched++;
                                continue;

                            } else {
                                JOptionPane.showMessageDialog(this, cellvalue
                                        + " is in incorrect Format\nFollow (FirstName LastName)\nUsing any digits and special characters is not allowed",
                                        "Error in Name", JOptionPane.ERROR_MESSAGE);

                            }

                        break;
                        case 1:
                            String mobilenumberRegex = "^\\+\\d{2}\\s[6-9]\\d{9}$";
                            Pattern mobilenumberPattern = Pattern.compile(mobilenumberRegex);
                            Matcher mobileNumberMatcher = mobilenumberPattern.matcher(cellvalue);

                            if (mobileNumberMatcher.matches()) 
                            {
                                cellMatched++;
                                continue;

                            } else 
                            {
                                JOptionPane.showMessageDialog(this, cellvalue + " is in incorrect Format", "Error in Phone Number",
                                        JOptionPane.ERROR_MESSAGE);

                            }
                        break;
                        case 2:
                            String emailRegex = "^[A-Za-z_]([\\.A-Za-z0-9\\+-_]+)*@([A-Za-z_]([A-Za-z0-9-])*)(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z0-9]{2,})$";
                            Pattern emailPattern = Pattern.compile(emailRegex);
                            Matcher emailMatcher = emailPattern.matcher(cellvalue);
                            if (emailMatcher.matches()) 
                            { 
                                for(int m=0;(m<rowCount)&&(edupCounter!=1);m++)
                                {
                                    for(int n=m+1;(n<rowCount)&&(edupCounter!=1);n++)
                                    {
                                        String mm=((String) contacttable.getValueAt(m, 2)).trim();
                                        String nn=((String) contacttable.getValueAt(n, 2)).trim();
                                        if(mm.equalsIgnoreCase(nn))
                                        { 
                                        JOptionPane.showMessageDialog(this, mm + " is Duplicate", "Error in Email",
                                            JOptionPane.ERROR_MESSAGE);
                                        edupCounter++;
                                        }
                                    }
                                }
                                if(edupCounter==0)
                                {
                                    cellMatched++;
                                    continue;
                                }    
                            } else 
                            {
                                JOptionPane.showMessageDialog(this, cellvalue + " is in incorrect Format", "Error in Email",
                                JOptionPane.ERROR_MESSAGE);
                            }  
                        break;
                        case 3:
                            String dobRegex = "^([0-2][1-9]|[3][0-1]|10|20)/([0][1-9]|[1][0-2])/(17[0-9][0-9]|18[0-9][0-9]|19[0-9][0-9]|20[0-1][0-9]|202[0-3])$";
                            Pattern dobPattern = Pattern.compile(dobRegex);
                            Matcher dobMatcher = dobPattern.matcher(cellvalue);
                            if (dobMatcher.matches()) 
                            {
                                for(int m=0;(m<rowCount)&&(mdupCounter!=1);m++)
                                {
                                    for(int n=m+1;(n<rowCount)&&(mdupCounter!=1);n++)
                                    {
                                        String mm=((String) contacttable.getValueAt(m, 1)).trim();
                                        String nn=((String) contacttable.getValueAt(n, 1)).trim();
                                        if(mm.equalsIgnoreCase(nn))
                                        { 
                                        JOptionPane.showMessageDialog(this, mm + " is Duplicate", "Error in Phone Number",
                                            JOptionPane.ERROR_MESSAGE);
                                        mdupCounter++;
                                        }
                                    
                                
                                    }
                                }
                                if(mdupCounter==0)
                                {
                                    cellMatched++;
                                    continue;
                                }
                            } 
                            else 
                            {

                                JOptionPane.showMessageDialog(this, cellvalue + " is incorrect\nFollow dd/mm/yyyy",
                                        "Error in DOB", JOptionPane.ERROR_MESSAGE);
                            }
                        break;
                        case 4:
                            if (cellvalue.equalsIgnoreCase("male") || cellvalue.equalsIgnoreCase("female")) 
                            {
                                cellMatched++;
                                continue;
                            } else 
                            {
                                JOptionPane.showMessageDialog(this,
                                        cellvalue + " is incorrect\nGender can be either: Male or Female", "Error in Gender",
                                        JOptionPane.ERROR_MESSAGE);

                            }
                        break;
                        case 5:
                            String addressRegex = "(\\s\\s)";
                            Pattern addressPattern = Pattern.compile(addressRegex);
                            Matcher addressMatcher = addressPattern.matcher(cellvalue);
                            if (addressMatcher.find()) 
                            {

                                JOptionPane.showMessageDialog(this,
                                        cellvalue + " cannot contain multiple spaces", "Error in Address",
                                        JOptionPane.ERROR_MESSAGE);
                                
                                

                            } else 
                            {
                               cellMatched++;
                                continue;
                            }
                            break;
                    }
                }

            }
            
            if (cellMatched == (rowCount  * columnCount ))
            {
                reloadData();
                JOptionPane.showMessageDialog(this,"Data Updated Succssfully", "Success",
                                              JOptionPane.INFORMATION_MESSAGE);
                
            }
        }
        if(ae.getSource()==search)
        {
            
            String searchText=searchbox.getText();
            // loadData();
            SearchDialog sd=new SearchDialog(this,"Search",searchText,TModel,contacttable);
            sd.setVisible(true);

            
        }
    }

    public static void reloadData()
    {
            int rowCount=TModel.getRowCount();
            int columnCount=TModel.getColumnCount();
            try
            {
            File f=new File("C:\\Users\\Shruti\\OneDrive\\Desktop\\SL\\Advance Java\\Contact Management\\database.xml");
            FileOutputStream fout=new FileOutputStream(f);
            PrintWriter pw=new PrintWriter(fout);
            pw.print("<contact-list>\n");
            for(int i=0;i<rowCount;i++)
            {
                Vector <String> v=new Vector<>();
                for(int j=0;j<columnCount;j++)
                {
                    v.add(((String)contacttable.getValueAt(i, j)).trim());
                }
                pw.println("    <contact>");
                            pw.print("      <name>");
                                pw.print(v.elementAt(0));
                            pw.println("</name>");
                            pw.print("      <phone_number>");
                                pw.print(v.elementAt(1));
                            pw.println("</phone_number>");
                            pw.print("      <email>");
                                pw.print(v.elementAt(2));
                            pw.println("</email>");
                            pw.print("      <dob>");
                                pw.print(v.elementAt(3));
                            pw.println("</dob>");
                            pw.print("      <gender>");
                                pw.print(v.elementAt(4));
                            pw.println("</gender>");
                            pw.print("      <address>");
                                pw.print(v.elementAt(5));
                            pw.println("</address>");
                        pw.println("    </contact>");
            }
            pw.print("</contact-list>");
            pw.close();
            fout.close();
            }
            catch(IOException ioe)
            {
                System.out.println(ioe.getMessage());
            } 
    }

    

}
class SearchDialog extends Dialog implements ActionListener
{
    DefaultTableModel tModel,sModel;
    JTable contacttable,searchTable;
    String searchText;
    JButton close;
    int rowCount;
    int columnCount;
    SearchDialog(JFrame parent, String title,String text, DefaultTableModel tModel, JTable contacttable)
    {
        super(parent, title, true);
        this.searchText=text.trim();
        this.tModel=tModel;
        this.contacttable=contacttable;
        
        setLayout(new FlowLayout());
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                dispose();

            }
        });
        initialize();
        searchText();
        
    }
    public void initialize()
    {
        sModel=new DefaultTableModel();
        searchTable=new JTable(sModel);
        close=new JButton("Close");
        JScrollPane jsp=new JScrollPane(searchTable,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
        ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.add(jsp);
        this.add(close);
        setBounds(500, 300, 620, 300);
        rowCount=tModel.getRowCount();
        columnCount=tModel.getColumnCount();
        close.addActionListener(this);


        /*adding columns headers from the main table, rows cant be added without TableModel
        having column names*/
        for (int i = 0; i < columnCount; i++) 
            sModel.addColumn(tModel.getColumnName(i));
                
    }


        
    public void searchText()
    {
        if(!searchText.equals(" "))   
        {
         for(int i=0;i<rowCount;i++)
            {
                int count=0;
                for(int j=0;j<columnCount&&count==0;j++)
                {
                    String value=((String)(contacttable.getValueAt(i, j))).trim();
                    if(value.contains(searchText))
                    {
                        count++;
                    
                        Vector <String> v=new Vector<String>();
                        for(int k=0;k<columnCount;k++)
                        {

                            v.add(((String)(contacttable.getValueAt(i, k))).trim());
                        }
                        sModel.addRow(v);

                    }
                }
            }
            this.setTitle(sModel.getRowCount()+" Results Found");
            if(sModel.getRowCount()==0)
            {
                   
                JOptionPane.showMessageDialog(this, "No entires found", "0 Results", JOptionPane.WARNING_MESSAGE);
                //dispose(); doubt
            }
        }
        
           
    }
    public void actionPerformed(ActionEvent ae) 
    {
        if(ae.getSource()==close)
        {
            dispose();
        }
    }
}
class AddContactForm extends Dialog implements ActionListener
{
    JLabel nameLabel,phone_numberLabel,addressLabel,emailLabel,dobLabel,genderLabel;
    JTextField name,phone_number,address,email,dob;
    JButton save,clear,cancel;
    ButtonGroup gender=new ButtonGroup();
    JRadioButton male,female;
    String selectedGender;
    DefaultTableModel tModel;
    JTable contacttable;
    boolean emailCheck,phone_numberCheck,nameCheck,dobCheck,addressCheck;
    AddContactForm(JFrame parent, String title, DefaultTableModel tModel, JTable contacttable)
    {
        super(parent, title, true);
        this.tModel=tModel;
        this.contacttable=contacttable;
        setLayout(new FlowLayout());
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                dispose();

            }
        });
        initialize();
    }
    public void actionPerformed(ActionEvent ae) 
    {
        if(ae.getSource()==male||ae.getSource()==female)
        {
            selectedGender=ae.getActionCommand();
        }
        if(ae.getSource()==cancel)
        {
        dispose();
        }
       
        if(ae.getSource()==clear)
        {
            name.setText("");
            phone_number.setText("+91 ");
            email.setText("");
            dob.setText("");
            address.setText("");
            gender.clearSelection();
        }
        if(ae.getSource()==save)
        {
            if(!(name.getText().isEmpty()||phone_number.getText().isEmpty()||
               email.getText().isEmpty()||dob.getText().isEmpty()||
               (male.isSelected()==false&&female.isSelected()==false)||address.getText().isEmpty()))
            {
                Vector <String> v=new Vector<>();
                v.add(name.getText());
                v.add(phone_number.getText());
                v.add(email.getText());
                v.add(dob.getText());
                v.add(selectedGender);
                v.add(address.getText());
                //System.out.println(v); 
                //Email Regex
                String emailRegex="^[A-Za-z_]([\\.A-Za-z0-9\\+-_]+)*@([A-Za-z_]([A-Za-z0-9-])*)(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z0-9]{2,})$";
                Pattern emailPattern=Pattern.compile(emailRegex);
                Matcher emailMatcher=emailPattern.matcher(v.elementAt(2));
                if(emailMatcher.matches())
                {
                    int emailDup=0;
                    for(int i=0;i<tModel.getRowCount();i++)
                    {
                        
                        if((((String)contacttable.getValueAt(i, 2)).trim()).equalsIgnoreCase(v.elementAt(2)))
                        {
                            emailDup++;
                            break;
                        }
                    }
                    if(emailDup==0)
                        emailCheck=true;
                    else
                    {
                      JOptionPane.showMessageDialog(this, "Contact with "+v.elementAt(2)+" already exists","Duplicate Email" , JOptionPane.WARNING_MESSAGE);
                      emailCheck=false;                    
                    } 
                }
                else
                {
                  emailCheck=false;
                  JOptionPane.showMessageDialog(this, "Email entered is not correct", "Error", JOptionPane.ERROR_MESSAGE);  
                }
                //Phone Number Regex
                String mobilenumberRegex="^\\+\\d{2}\\s[6-9]\\d{9}$";
                Pattern mobilenumberPattern=Pattern.compile(mobilenumberRegex);
                Matcher mobileNumberMatcher=mobilenumberPattern.matcher(v.elementAt(1));
                if(mobileNumberMatcher.matches())
                {
                    int mobileDup=0;
                    for(int j=0;j<tModel.getRowCount();j++)
                    {
                        
                        if((((String)contacttable.getValueAt(j, 1)).trim()).equalsIgnoreCase(v.elementAt(1)))
                        {
                            mobileDup++;
                            break;
                        }
                    }
                    if(mobileDup==0)
                        phone_numberCheck=true;
                    else
                    {
                      JOptionPane.showMessageDialog(this, "Contact with "+v.elementAt(1)+" already exists","Duplicate Phone Number" , JOptionPane.WARNING_MESSAGE);
                      phone_numberCheck=false;
                    } 
                }
                else
                {
                 phone_numberCheck=false;
                 JOptionPane.showMessageDialog(this, "Mobile Number is not correct", "Error", JOptionPane.ERROR_MESSAGE);  
                }

                //Name Regex
                String nameRegex="^[a-zA-Z]+\\s*[a-zA-Z]*$";
                Pattern namePattern=Pattern.compile(nameRegex);
                Matcher nameMatcher=namePattern.matcher(v.elementAt(0));
                if(nameMatcher.matches())
                {
                    nameCheck=true;
                }
                else
                {
                    nameCheck=false;
                    JOptionPane.showMessageDialog(this, "Name is in incorrect Format\nFollow (FirstName LastName)\nUsing any digits and special characters is not allowed", "Error", JOptionPane.ERROR_MESSAGE);  
                }

                //DOB Regex
                String dobRegex="^([0-2][1-9]|[3][0-1]|10|20)/([0][1-9]|[1][0-2])/(17[0-9][0-9]|18[0-9][0-9]|19[0-9][0-9]|20[0-1][0-9]|202[0-3])$";
                Pattern dobPattern=Pattern.compile(dobRegex);
                Matcher dobMatcher=dobPattern.matcher(v.elementAt(3));
                if(dobMatcher.matches())
                {
                    dobCheck=true;
                }
                else
                {
                    dobCheck=false;
                    JOptionPane.showMessageDialog(this, "DOB is incorrect\nFollow dd/mm/yyyy", "Error", JOptionPane.ERROR_MESSAGE);  
                }


                //Address Regex
                String addressRegex="(\\s\\s)";
                Pattern addressPattern=Pattern.compile(addressRegex);
                Matcher addressMatcher=addressPattern.matcher(v.elementAt(5));
                if(addressMatcher.find())
                {
                    addressCheck=false;
                    JOptionPane.showMessageDialog(this, "Address cannot contain consecutive spaces", "Error", JOptionPane.ERROR_MESSAGE);  
                }
                else
                {
                    addressCheck=true;
                }
                if(emailCheck&&phone_numberCheck&&nameCheck&&dobCheck&&addressCheck)
                {
                try
                    {
                        tModel.addRow(v);
                        this.dispose();
                        super.dispose();
                        File f=new File("C:\\Users\\Shruti\\OneDrive\\Desktop\\SL\\Advance Java\\Contact Management\\database.xml");
                        FileOutputStream fout=new FileOutputStream(f,true);
                        PrintWriter pw=new PrintWriter(fout);
                        pw.println("<contact>");
                            pw.print("    <name>");
                                pw.print(v.elementAt(0));
                            pw.println("</name>");
                            pw.print("    <phone_number>");
                                pw.print(v.elementAt(1));
                            pw.println("</phone_number>");
                            pw.print("    <email>");
                                pw.print(v.elementAt(2));
                            pw.println("</email>");
                            pw.print("    <dob>");
                                pw.print(v.elementAt(3));
                            pw.println("</dob>");
                            pw.print("    <gender>");
                                pw.print(v.elementAt(4));
                            pw.println("</gender>");
                            pw.print("    <address>");
                                pw.print(v.elementAt(5));
                            pw.println("</address>");
                        pw.println("</contact>");
                        pw.close();
                        fout.close();   
                        MyContactManager.reloadData();
                        JOptionPane.showMessageDialog(this, "Contact Saved Successfully", "Success", JOptionPane.INFORMATION_MESSAGE);

                    }
                    catch(IOException ioe)
                    {
                        System.out.println("IO Exception Caught");
                    }
                }
            }
            else
            {
            JOptionPane.showMessageDialog(this, "One More Fields are empty", "Error", JOptionPane.ERROR_MESSAGE);
            }

        }
        
    }

    public void initialize()
    {
        //Labels
        nameLabel=new JLabel("Name*:");
        phone_numberLabel=new JLabel("Phone Number*:");
        addressLabel=new JLabel("Address*:");
        dobLabel=new JLabel("Date of Birth*:");
        emailLabel=new JLabel("Enter email*:");
        genderLabel=new JLabel("Gender*");

        //TextFields
        name=new JTextField(10);
        phone_number=new JTextField("+91 ",10);
        address=new JTextField(10);
        dob=new JTextField(10);
        email=new JTextField(10);

        //RadioButtons
        male=new JRadioButton("Male");
        female=new JRadioButton("Female");


        save=new JButton("Save Contact");
        clear=new JButton("Clear");
        cancel=new JButton("Cancel");
        


        //Adding Radio Buttons to Button Groups
        gender.add(male);
        gender.add(female);
        
        


        this.add(nameLabel);
        this.add(name);
        this.add(phone_numberLabel);
        this.add(phone_number);
        this.add(addressLabel);
        this.add(address);
        this.add(dobLabel);
        this.add(dob);
        this.add(emailLabel);
        this.add(email);
        this.add(genderLabel);
        this.add(male);
        this.add(female);
        this.add(save);
        this.add(clear);
        this.add(cancel);
        male.addActionListener(this);
        female.addActionListener(this);
        save.addActionListener(this);
        clear.addActionListener(this);
        cancel.addActionListener(this);
        setBounds(500, 300, 620, 300);
    }
}

public class ContactManagement 
{

    public static void main(String[] args) {
        new MyContactManager("Contact Management");
    }
}